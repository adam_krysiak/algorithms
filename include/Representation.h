/*
 * Representation.h
 *
 *  Created on: 09.04.2016
 *      Author: adam
 */

#ifndef REPRESENTATION_H_
#define REPRESENTATION_H_

class Representation {
public:
	int startingPoint;
	Representation() = default;
	virtual void getMST() = 0;
	virtual void getShortestPath() = 0;
	virtual void addNode()=0;
	virtual void addOneWayNeighbour(int a, int b, int w) = 0;
	virtual void addBothWayNeighbour(int a, int b, int w) = 0;
	virtual void display()=0;
	void addFromFileMST();
	void addFromFileShortestPath();
	void addRandom(int howMany = 15,int percentageDensity= 25,bool trueIfMST = false);
	virtual ~Representation() = default;
};



#endif /* REPRESENTATION_H_ */
