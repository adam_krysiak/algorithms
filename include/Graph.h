/*
 * graph.h
 *
 *  Created on: 16.04.2016
 *      Author: adam
 */

#ifndef INCLUDE_GRAPH_H_
#define INCLUDE_GRAPH_H_

#include <iostream>
#include <set>
#include <vector>

class Graph{
public:
	std::set<int> nodes;
	std::vector<std::vector<int>> edges;
	int weight;
	int getSize();
	bool equals(const Graph & b);
	void display();
	static Graph MakeGraphFromVector(std::vector<int> edge);
	static int FindInGraph(std::vector<Graph>&graphTree, int element);
	static void connectTwoGraphs(Graph & a, Graph & b);
	static void displayGraphTree(std::vector<Graph> tree);
	static bool isInVector(const std::vector<int> &searched,const std::vector<std::vector<int>> &vector);



};


#endif /* INCLUDE_GRAPH_H_ */
