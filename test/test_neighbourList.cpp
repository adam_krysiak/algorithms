#include <gtest/gtest.h>
#include <NeighbourList.h>
#include <list>
#include <vector>

TEST(neighbourList, dijastryAlgorithm)
{
	NeighbourList a;

	for (int i = 0; i < 6; i++)
		a.addNode();

	for (int i = 0; i < a.elements.size(); i++)
		EXPECT_EQ(a.elements[i]->number, i);

	a.setNeighbours(0, 1, 1);
	a.setNeighbours(0, 3, 2);

	a.setNeighbours(1, 2, 3);
	a.setNeighbours(1, 3, 3);

	a.setNeighbours(2, 4, 1);
	a.setNeighbours(2, 5, 3);

	a.setNeighbours(3, 4, 2);

	a.setNeighbours(4, 5, 4);

	//a.display();
	std::vector<int> c =
	{ 3, 4, 5 };
	auto djiskryA = a.Dijkstry(3, 5);
	auto djiskryIter = djiskryA.begin();
	for (int i = 0; i < djiskryA.size(); i++, djiskryIter++)
		EXPECT_EQ(*(djiskryIter), c[i]);

}

TEST(neighbourList, GetListOfEdgesWithWeigth)
{
	NeighbourList a;

	for (int i = 0; i < 7; i++)
		a.addNode();

	for (int i = 0; i < a.elements.size(); i++)
		EXPECT_EQ(a.elements[i]->number, i);

	a.setNeighbours(0, 1, 7);

	a.setNeighbours(0, 3, 5);

	a.setNeighbours(1, 2, 8);
	a.setNeighbours(1, 3, 9);
	a.setNeighbours(1, 4, 7);

	a.setNeighbours(2, 4, 5);

	a.setNeighbours(3, 4, 15);
	a.setNeighbours(3, 5, 6);

	a.setNeighbours(4, 6, 9);

	a.setNeighbours(5, 6, 11);

	//a.display();
	auto listOfEdges = a.GetListOfEdgesWithWeigth();
	a.Kruskal();

}

