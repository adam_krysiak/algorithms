/*
 * test_matrix.cpp
 *
 *  Created on: 15.04.2016
 *      Author: adam
 */

#include <gtest/gtest.h>
#include <Matrix.h>
#include <NeighbourList.h>
#include <Stoper.h>
#include <fstream>
#include <iostream>



 TEST(Matrix, dijastryAlgorithm){

 Matrix a;

 for(int i=0;i<6;i++)
 a.addNode();


 a.setNeighbours(0,1,1);
 a.setNeighbours(0,3,2);

 a.setNeighbours(1,2,3);
 a.setNeighbours(1,3,3);

 a.setNeighbours(2,4,1);
 a.setNeighbours(2,5,3);

 a.setNeighbours(3,4,2);

 a.setNeighbours(4,5,4);


 //	a.display();
 std::vector<int>c = {3,4,5};
 auto djiskryA = a.Dijkstry(3,5);
 auto djiskryIter = djiskryA.begin();
 for(int i=0;i<djiskryA.size();i++, djiskryIter++)
 EXPECT_EQ(*(djiskryIter),c[i] );

 }

